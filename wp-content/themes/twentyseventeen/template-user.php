<?php
/**
 * The template for displaying contact page.
 *
 * Template Name: User API
 *
 */
?>
<?php 

header('Content-type: application/json;');
header("Access-Control-Allow-Origin: *");


$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$user = wp_authenticate( $username, $password );

if ( is_wp_error( $user ) ) {
	$wp_json_basic_auth_error = $user;
	echo json_encode(['error' => false, 'message' => 'Authentication Failed']); exit;
}


$uniqueId = $_POST['uniqueid'] ;

if($uniqueId){
	 global $wpdb; 
	
	$results = $wpdb->get_row('SELECT `user_id` FROM `'.$wpdb->prefix.'usermeta` WHERE `meta_value` LIKE "'.$uniqueId.'"');
	$userId  =  $results->user_id;
	$checkLog =	get_user_meta($userId ,'loggedin', true);

	$date = date("d-m-Y H:i:s");
	if(!empty($userId) && empty($checkLog) ){
		//update_user_meta($userId, 'loggedin' ,'1');
		//update_user_meta($userId, 'loginDate', $date);
		
        
		$hotelArray = array();
		
		$userHotels = get_field('user_hotels','user_'.$userId);

		$defaultImage = array('landing_image' => get_field('landing_image','options'),
        		'logo' => get_field('logo','options')		
        		 );

		$qry_args = array(
        'post_status' => 'publish', // optional
        'post_type' => 'hotels', // Change to match your post_type
        'posts_per_page' => -1, // ALL posts
        );

        $the_query = new WP_Query( $qry_args );
		if ( $the_query->have_posts() ) {

        	while ( $the_query->have_posts() ) : $the_query->the_post(); 
        		/*Basic Info */
        	
        		$postID = get_the_ID();
        		$title = get_the_title();
        		$description = get_field('content_description', $postID);
        		$address = get_field('address', $postID);
        		$cityId = get_field('select_city', $postID);
        		$cityData = get_term($cityId);
        		$brandID = get_field('select_brand', $postID);
        		$brandName = get_the_title($brandID);
        		$capacity = get_field('capacity', $postID);
        		$userArray = get_field('user', $postID);
        		$feat_image = wp_get_attachment_url( get_post_thumbnail_id($postID) );
        		
        		$basicInfo = array('id' => $postID,'title' =>$title, 'city'=>$cityData->name, 'capacity'=>$capacity, 'brand' =>$brandName,
        		'image' =>$feat_image
        		);
       
    
       			$hotelArray[] = array('basic_info' => $basicInfo);
       	
       		endwhile; 

     	}

      // /* Getting all the Amennites*/

      // $termsArray = get_terms( array(
      //     'taxonomy' => 'amenities',
      //     'hide_empty' => false
      // ) );

      // if ( ! empty( $termsArray ) && ! is_wp_error( $termsArray ) ) {  
      //     $amenityInfo = array();
      //     foreach ( $termsArray as $term ) {
      //         $amenityID = $term->term_id;
      //         $amenityIcon = get_field('icon', 'amenities_'.$amenityID);
      //         $amenityInfo[] = array('amenity_id' => $amenityID, 'amenity_icon'=>$amenityIcon);
      //     }
      // }    
		


    $jsonvar[] = array('status' => True , 'message' => 'Logging in' ,'hotel_id' =>$userHotels , 'default_image'=>$defaultImage,'data' => $hotelArray);
		echo json_encode($jsonvar);
		exit();
	}
	else{
		$jsonvar[] = array('status' => false , 'message' => 'Invalid details');
	    echo json_encode($jsonvar);
	    exit();
	}	
}
else{
	  $jsonvar[] = array('status' => false , 'message' => 'You must include the Required parameters');
      echo json_encode($jsonvar);
      exit();
}

echo json_encode($jsonvar);