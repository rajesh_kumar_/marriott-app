<?php
/**
 * The template for displaying contact page.
 *
 * Template Name: Loyalty Program API
 *
 */
?>
<?php  

header('Content-type: application/json;');
header("Access-Control-Allow-Origin: *");


$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$user = wp_authenticate( $username, $password );

if ( is_wp_error( $user ) ) {
	$wp_json_basic_auth_error = $user;
	echo json_encode(['error' => false, 'message' => 'Authentication Failed']); exit;
}


/* Getting all the Reward Categories*/

      $termsArray = get_terms( array(
          'taxonomy' => 'reward-category',
          'hide_empty' => false
      ) );

      if ( ! empty( $termsArray ) && ! is_wp_error( $termsArray ) ) {  
          $rewardCatInfo = array();
          foreach ( $termsArray as $term ) {
          	//print_r($term);
              $rewardCatID = $term->term_id;
              $rewardCatName = $term->name;
              $rewardCatDesc = $term->description;
              $rewardCatIcon = get_field('icon', 'reward-category_'.$term->term_id);
              $rewardCatInfo[] = array('reward_cat_id' => $rewardCatID, 'reward_cat_icon'=>$rewardCatIcon, 'reward_cat_name' =>$rewardCatName ,'reward_cat_desc'=>$rewardCatDesc);
          }
      }    

 
   /*Getting rewads page info*/

    $rewardsPageInfo = get_fields(459);
    
    /*Getting rewards Detail*/
    
     $args = array(
        'numberposts' => -1,
        'post_type'   => 'rewards',
        'post_status' => array('publish')
      ); 

     $rewardsPost = get_posts($args);
     $rewardsDetail = array();
     foreach($rewardsPost as $rewards){
     	$rewardsDetail[] = get_fields($rewards->ID);
     }

  	$rewardsFinalArray = array('rewards-cat' =>$rewardCatInfo, 
  	                         'rewards-page'=> $rewardsPageInfo, 
  	                         'rewards-detail'=> $rewardsDetail
  	                    );

    $jsonvar[] = array('status' => True  ,'rewards' =>$rewardsFinalArray);
	echo json_encode($jsonvar);
	exit();