<?php
/**
 * The template for displaying contact page.
 *
 * Template Name: Shadi By Marriott API
 *
 */
?>
<?php 

header('Content-type: application/json;');
header("Access-Control-Allow-Origin: *");


$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$user = wp_authenticate( $username, $password );

if ( is_wp_error( $user ) ) {
	$wp_json_basic_auth_error = $user;
	echo json_encode(['error' => false, 'message' => 'Authentication Failed']); exit;
}


  $shadibyMarriott = get_fields(423);

if($shadibyMarriott)
{
  $jsonvar[] = array('status' => True ,'data' =>$shadibyMarriott );
  echo json_encode($jsonvar);
  exit();

} 

else{
	$jsonvar[] = array('status' => false ,'message' => 'page not exist' );
  echo json_encode($jsonvar);
  exit();
}
