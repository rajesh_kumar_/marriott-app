<?php
/**
 * The template for displaying contact page.
 *
 * Template Name: Maps API
 *
 */
?>
<?php 

header('Content-type: application/json;');
header("Access-Control-Allow-Origin: *");


$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$user = wp_authenticate( $username, $password );

if ( is_wp_error( $user ) ) {
	$wp_json_basic_auth_error = $user;
	echo json_encode(['error' => false, 'message' => 'Authentication Failed']); exit;
}

   /*Getting Main image of map*/

    $mapUrl = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));

   /*Getting the all the city map*/

    $args = array(
        'numberposts' => -1,
        'post_type'   => 'maps',
        'post_status' => array('publish')
      ); 

    $mapsArray = get_posts($args);
    $mapsTitle = array();	
    $mapsDetail = array();
    $mapsFinalArray = array();
    foreach($mapsArray as $maps){
     	$mapsDetail[] = array(
     		'title'	 =>  get_the_title($maps->ID),
	     	'no_of_hotels' => get_field('no_of_hotels', $maps->ID),
	      	'x_coordinate' => get_field('x_coordinate', $maps->ID),
			'y_coordinate' => get_field('y_coordinate', $maps->ID),
			'city_name_x_coordinate' => get_field('city_name_x_coordinate', $maps->ID),
			'city_name_y_coordinate' => get_field('city_name_y_coordinate', $maps->ID),
			'city_image' => get_field('city_image', $maps->ID),
			'promote' => get_field('promote', $maps->ID),
			'city_hotels' => get_field('city_hotels', $maps->ID)
		);
	}
	
	if($mapsDetail){
	  
	  $jsonvar[] = array('status' => True ,'main_map'=> $mapUrl,'data' =>$mapsDetail);
	  echo json_encode($jsonvar);
	  exit();
	}

	else{
		$jsonvar[] = array('status' => false ,'message' => 'Maps does not exist' );
	  echo json_encode($jsonvar);
	  exit();
	}
