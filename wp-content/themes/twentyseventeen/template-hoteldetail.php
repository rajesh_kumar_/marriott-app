<?php
/**
 * The template for displaying contact page.
 *
 * Template Name: Hotel API
 *
 */
?>
<?php 

header('Content-type: application/json;');
header("Access-Control-Allow-Origin: *");


$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$user = wp_authenticate( $username, $password );

if ( is_wp_error( $user ) ) {
	$wp_json_basic_auth_error = $user;
	echo json_encode(['error' => false, 'message' => 'Authentication Failed']); exit;
}


$hotelId = $_POST['hotel_id'];


if($hotelId){

	$hotelArray = array();  
    $amenity = array();
    $highlight = array();
    $roomInfo = array();
    $roomAmenityName = array();
    $roomFeature = array();
    $floorPlanDetails = array();
    $hotelDetailArray = get_fields($hotelId);
    $hotelAmenityFinal = array();
  
    //print_r($hotelDetailArray);
	$hotelAmenitiesArray = $hotelDetailArray['hotel_amenities'];
    $highlightsArray = $hotelDetailArray['hotel_highlights'];
    $roomInfoArray = $hotelDetailArray['room_information'];
    $floorPlanArray = $hotelDetailArray['floor_plan_details'];

    foreach ($hotelAmenitiesArray as  $hotelamenity) {
     
        $hotelAmenityIcon = get_field('icon', 'amenities_'.$hotelamenity->term_id);
        $hotelAmenityFinal[] = array('hotelamenity_name'=>$hotelamenity->name, 'hotelamenity_name_image'=>$hotelAmenityIcon);
    }


    foreach ($highlightsArray as  $highlights) {
      $highlight[] = $highlights['highlights'];
    }

    foreach ($roomInfoArray as $roomDetail) {
        $roomAmenityArray =  $roomDetail['room_amenities'];
        $amenityFinal = array();
        $featureFinal = array();
        foreach ($roomAmenityArray as $roomAmenity) {
           //print_r($roomvalue);
           $amenityIcon = get_field('icon', 'amenities_'.$roomAmenity->term_id);
           $amenityFinal[] = array('amenity_name'=>$roomAmenity->name, 'amenity_image'=>$amenityIcon);
        }
        $roomFeatureArray = $roomDetail['room_features'];
       // print_r($roomFeatureArray);

        foreach ($roomFeatureArray as $roomFeatures) {
            $featureIconId = $roomFeatures['f_icon'];
            $featureName = $roomFeatures['feature_heading'];
            $featureIcon = get_field('icon', 'features_'.$featureIconId);
           
            $featureFinal[] = array('feature_name'=>$featureName, 'faeture_image'=>$featureIcon);

        }



     $roomInfo[] = array(
        'heading' => $roomDetail['room_heading'],
        'data' =>array(
            'room_image_gallery'=> $roomDetail['room_image_gallery'],
            'room_feature_heading'=> $roomDetail['room_feature_heading'],
            'room_description' => strip_tags($roomDetail['room_description']),
            'room_features' => $featureFinal,
            'room_amenities' => $amenityFinal   



        ),
       
      );  
    }


    foreach ($floorPlanArray as $floorPlan) {
      $floorPlanDetails[] = array(
        'metting_rooms' => $floorPlan['meeting_rooms'],
        'dimensions' => $floorPlan['dimensions'],
        'area' => $floorPlan['area'],
        'theater' => $floorPlan['theater'],
        'theater_image' => $floorPlan['theater_image'],
        'classroom' => $floorPlan['classroom'],
        'classroom_image' => $floorPlan['classroom_image'],
        'conference' => $floorPlan['conference'],
        'conference_image' => $floorPlan['conference_image'],
        'u-shape' => $floorPlan['u-shape'],
        'ushape_image' => $floorPlan['ushape_image'],
        'reception' => $floorPlan['reception'],
        'reception_image' => $floorPlan['reception_image']

        );
    }

    //print_r($roomAme);
    $city = get_term( $hotelDetailArray['select_city'], 'location');
    $tempArray = 
      array(
        'title' => get_the_title($hotelId),
        'hotel_desc' => $hotelDetailArray['content_description'],
        'address' =>$hotelDetailArray['address'],
        'city' => $city->name,
        'capacity'=> $hotelDetailArray['capacity'],
        'brand' => get_the_title($hotelDetailArray['select_brand']),
        'airport_name'=> $hotelDetailArray['airport_name'],
        'airport_distance' => $hotelDetailArray['airport_distance'],
        'airport_duration' => $hotelDetailArray['airport_duration'],
        'map_image' => $hotelDetailArray['map_image'],
        'airport_direction' =>$hotelDetailArray['airport_direction'],
        'hotel_map_link' =>$hotelDetailArray['hotel_map_link'],
        // 'latitude' => $hotelDetailArray['latitude'],
        // 'longitude'=> $hotelDetailArray['longitude'],
        // 'airport_latitude' => $hotelDetailArray['airport_latitude'],
        // 'airport_longitude'=> $hotelDetailArray['airport_longitude'],
        'phone_number' => $hotelDetailArray['phone_number'],
        'email' => $hotelDetailArray['email'],
        'facebook_link'=> $hotelDetailArray['facebook_link'],
        'twitter_link' => $hotelDetailArray['twitter_link'],
        'instagram_link' => $hotelDetailArray['instagram_link'],
        'gplus_link' => $hotelDetailArray['gplus_link'],
        'hotel_images'=>$hotelDetailArray['hotel_images'],
        'hotel_amenities' => array('heading' => $hotelDetailArray['amenity_heading'] , 'data' => $hotelAmenityFinal),
        'rooms_heading' => $hotelDetailArray['rooms_heading'],
        'no_of_rooms' => $hotelDetailArray['no_of_rooms'],
        'no_of_suits' => $hotelDetailArray['no_of_suits'],
        // 'highlights_heading'=> $hotelDetailArray['highlights_heading'],
        'highlights' => array('heading'=> $hotelDetailArray['highlights_heading'], 'data' => $highlight),
        'key_contacts' => array('heading' => $hotelDetailArray['hotel_contacts_heading'] ,'data' => $hotelDetailArray['key_contacts']),
        
        'policy_info' => array('heading' =>$hotelDetailArray['policy_heading'], 
                    'data' => array('checkin_time' => $hotelDetailArray['checkin_time'], 
                                    'checkout_time'=> $hotelDetailArray['checkout_time'],
                                     'policy_desc' => $hotelDetailArray['description']   
                                )
                         ),

        'spa_info' => array('heading'=>$hotelDetailArray['spa_heading'], 'data'=>$hotelDetailArray['spa_membership']), 

        'fitness_info' => array('heading'=>$hotelDetailArray['fitness_center_heading'], 
                                'data'=> $hotelDetailArray['fitness_center']
                            ),

        'payment_info' => array('heading'=> $hotelDetailArray['payment_heading'],
                                'data'=> $hotelDetailArray['payment_details']
                            ),
        'safety_info' =>array('heading' => $hotelDetailArray['safety_feature_heading'],
                              'data' => $hotelDetailArray['safety_features']

                            ),
        'parking_info' => array('heading'=> $hotelDetailArray['parking_option_heading'], 'data'=> $hotelDetailArray['parking_options'] ),
        
        'room_information'=> $roomInfo,
        'meeting_information'=> array('data'=> array(
        'meeting_events_images'=> $hotelDetailArray['meeting_events_images'],
        'meeting_events_info' => array('heading' => $hotelDetailArray['number_of_meeting_heading'],
                                        'data' => $hotelDetailArray['number_of_meeting_rooms']),

        'room_highlights_info' => array( 'heading'=>$hotelDetailArray['room_highlights_heading'],'data'=>$hotelDetailArray['room_highlights']),
        
        'floor_plan_info' => array('heading' => $hotelDetailArray['floor_plan_heading'],
                                   'data' =>array(
                                                'floor_plan_columns' => $hotelDetailArray['floor_plan_columns'],
                                                'floor_plan_details'=>$hotelDetailArray['floor_plan_details']),
                                                'floor_plan_image' => $hotelDetailArray['floor_plan_image']
                                   ),

        'f&bsetup_info' => array('heading'=>$hotelDetailArray['f&bsetup_heading'], 
                            'data'=> array('menu' => $hotelDetailArray['menu'],'theme' => $hotelDetailArray['theme']  )
                        )
      
        

        )),
        
        'restaurant_details_info' => $hotelDetailArray['restaurant_details'],
        
        'local_highlights_info' => array('heading' => $hotelDetailArray['usp_heading'], 'data'=>$hotelDetailArray['local_highlights']),

        'city_location_info' => array('heading' => $hotelDetailArray['city_location_heading'], 'data' =>array('c_locations'=>$hotelDetailArray['city_locations'], 'c_map'=>$hotelDetailArray['city_map'])), 

        'local_attraction_info' => array('heading'=>$hotelDetailArray['local_attraction_heading'],'data'=>$hotelDetailArray['local_attraction_details']),
      
        'price_info'=> array('data'=> array('tax'=>$hotelDetailArray['tax'], 'price_detail'=>$hotelDetailArray['price_detail']))
        );

        

    $jsonvar[] = array('status' => True  ,'hotel_detail' =>$tempArray);
		echo json_encode($jsonvar);
		exit();
	}

else{
	  $jsonvar[] = array('status' => false , 'message' => 'You must include the Required parameters');
      echo json_encode($jsonvar);
      exit();
}

echo json_encode($jsonvar);