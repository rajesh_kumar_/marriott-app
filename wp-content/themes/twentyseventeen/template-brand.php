<?php
/**
 * The template for displaying contact page.
 *
 * Template Name: Brand API
 *
 */
?>
<?php 

header('Content-type: application/json;');
header("Access-Control-Allow-Origin: *");


$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$user = wp_authenticate( $username, $password );

if ( is_wp_error( $user ) ) {
	$wp_json_basic_auth_error = $user;
	echo json_encode(['error' => false, 'message' => 'Authentication Failed']); exit;
}

    $args = array(
        'numberposts' => -1,
        'post_type'   => 'brands',
        'post_status' => array('publish')
      ); 

  $brandPost = get_posts($args);
  $brandFinalArray = array();
  foreach ($brandPost as $brands) {
      $locations =array();
      $category = array();
      $bId = $brands->ID;
      $bTitle = $brands->post_title;
      $bDesciption = get_field('b_description',$bId);
      $bFacebook = get_field('facebook_link',$bId);
      $bTwitter = get_field('twitter_link',$bId);
      $bInsta = get_field('instagram_link',$bId);
      $bGplus = get_field('gplus_link',$bId);
      $bLocations = get_field('present_in',$bId);
      $bCategoryArray = get_field('brand_category',$bId);
      $blogo = get_field('brand_logo',$bId);

      $bCategoryImage =  wp_get_attachment_image_src(get_post_thumbnail_id($bId), 'full');

      foreach ($bLocations as $key => $value) {
          $locations[] = $value->name;
      }

      $bCategoryId = $bCategoryArray->term_id;
      $bCategoryName = $bCategoryArray->name;
      $bCategoryIcon =get_field('icon',"brand-category_$bCategoryId");  
      $bCategoryDesc = $bCategoryArray->description;
      $brandFinalArray[] = array(
                              'brand-data'=>array(
                                'brand_title' => $bTitle, 
                                'brand_location'=> $locations,
                                'brand_desc'=> $bDesciption,
                                'brand-logo'=> $blogo,
                                'brand_image'=> $bCategoryImage[0],
                                'brand_fb' => $bFacebook,
                                'brand_twitter'=> $bTwitter,
                                'brand_insta'=> $bInsta,
                                'brand_gplus'=> $bGplus
                              ),  
                              'category'=>array(
                                 'cat_name'=> $bCategoryName,
                                 'cat_image'=> $bCategoryIcon,
                                 'cat_desc'=>$bCategoryDesc
                              )
                          );
  } 

$jsonvar[] = array('status' => True ,'brand_info' =>$brandFinalArray );
echo json_encode($jsonvar);
exit();
	