<?php
/**
 * The template for displaying contact page.
 *
 * Template Name: Meetings Imagined API
 *
 */
?>
<?php 

header('Content-type: application/json;');
header("Access-Control-Allow-Origin: *");


$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$user = wp_authenticate( $username, $password );

if ( is_wp_error( $user ) ) {
	$wp_json_basic_auth_error = $user;
	echo json_encode(['error' => false, 'message' => 'Authentication Failed']); exit;
}


$meetingsImagined = get_fields(436);
  
if($meetingsImagined){
  
  $jsonvar[] = array('status' => True ,'data' =>$meetingsImagined );
  echo json_encode($jsonvar);
  exit();
}

else{
	$jsonvar[] = array('status' => false ,'message' => 'Page not exist' );
  echo json_encode($jsonvar);
  exit();
}
