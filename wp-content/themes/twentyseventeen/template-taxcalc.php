<?php
/**
 * The template for displaying contact page.
 *
 * Template Name: Tax Calculator API
 *
 */
?>
<?php 

header('Content-type: application/json;');
header("Access-Control-Allow-Origin: *");


$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];
$user = wp_authenticate( $username, $password );

if ( is_wp_error( $user ) ) {
	$wp_json_basic_auth_error = $user;
	echo json_encode(['error' => false, 'message' => 'Authentication Failed']); exit;
}

$hotelId = $_POST['hotel_id'];


if($hotelId){

  $priceInfo = get_field('price_detail', $hotelId);

  $jsonvar[] = array('status' => True ,'tax_info' =>$priceInfo );
  echo json_encode($jsonvar);
  exit();

} 

else{
	$jsonvar[] = array('status' => false ,'message' => 'wrong hotel id' );
  echo json_encode($jsonvar);
  exit();
}
