<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'marriott');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h[stAPYtL{Us>,avFd~/]<(M Vg@U)!Vc3Ok1_T chd:e^B(i*Gr;T4s{rUH1RyA');
define('SECURE_AUTH_KEY',  'A-*ytI<VJ.LG0v>SNe>g;W2H$?*XC7v}TP1:~c>b@nM-Kc mQTMe$Qta<5 ^`FO&');
define('LOGGED_IN_KEY',    'tIG&8M:bl<Hp>|a#@6E8m2 vZ*C>_UeQBjWb2@?4BkvWpqpT?!aig6=JA-AIZy?:');
define('NONCE_KEY',        'K6$rpG8]^qq>r#2]WI:2XQ*59~i=#3.d(7)%$[=eXA%,cHGHfDtx7~mqcC$E1`?]');
define('AUTH_SALT',        'bT:gqjuUu}L7tZtUaP!P3VwPK?_SH8kMd,.{}kj2]MF!3<I~N6NLhij6.3[y,vzI');
define('SECURE_AUTH_SALT', '9Ko6sl=aiCt:8Z;HV<V*~2={[7$Ec0io`_[LB7dz@/CMI+PM:$qe!.>yq19_z?X#');
define('LOGGED_IN_SALT',   '!55w 02W 7IB)JgVf>}MA5a5l<x Jqd,uxsTByGJlo:IU{w#D3!|[SE[t0q>1w@c');
define('NONCE_SALT',       '.o<LHD@!g`}*x)hZ ~}ZF92_(/wijB(@P2.SR9/<ZWL2 y8>8J8(F mCWo5;<j)V');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mr_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
